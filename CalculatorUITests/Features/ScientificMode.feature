﻿Feature: Use scientifc mode

Scenario: Check the absolute value of negative number
	Given I have navigated to the scientific mode
	When I have entered number 43
		And I have pressed scientific button: negate
		And I have pressed scientific button: absolute
		And I have pressed '=' operator button
	Then I verify that the result is 43

Scenario: Test parenthesis 1st scenario
	Given I have navigated to the scientific mode
	When I have pressed scientific button: open parenthesis
		And I have entered number 2
		And I have pressed '+' operator button
		And I have entered number 2
		And I have pressed scientific button: close parenthesis
		And I have pressed '*' operator button
		And I have entered number 2
		And I have pressed '=' operator button
	Then I verify that the result is 8

Scenario: Test parenthesis 2nd scenario
	Given I have navigated to the scientific mode
	When I have entered number 2
		And I have pressed '+' operator button
		And I have pressed scientific button: open parenthesis
		And I have entered number 2
		And I have pressed '*' operator button
		And I have entered number 2
		And I have pressed scientific button: close parenthesis
		And I have pressed '=' operator button
	Then I verify that the result is 6