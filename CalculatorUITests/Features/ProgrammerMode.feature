﻿Feature: Use programmer mode

Scenario: Verify values in programmer mode
	Given I have navigated to the programmer mode
	When I have entered number 432
	Then I verify that the value of HEX is 1 B 0
		And I verify that the value of DEC is 432
		And I verify that the value of OCT is 6 6 0
		And I verify that the value of BIN is 0 0 0 1  1 0 1 1  0 0 0 0