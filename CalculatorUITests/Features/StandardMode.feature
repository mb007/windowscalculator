﻿Feature: Use standard mode

Scenario: Test multiply function
	Given I have navigated to the standard mode
	When I have entered number 21
		And I have pressed '*' operator button
		And I have entered number 21
		And I have pressed '=' operator button
	Then I verify that the result is 441
	When I have pressed 'cancel' operator button
		And I have opened history
		And I have selected history list item number 1
	Then I verify that the result is 441

Scenario: Test memory function
	Given I have navigated to the standard mode
		When I have entered number 21
			And I have pressed '*' operator button
			And I have entered number 10
			And I have pressed '=' operator button
			And I have pressed 'memory plus' operator button
			And I have entered number 15
			And I have pressed '*' operator button
			And I have entered number 10
			And I have pressed '=' operator button
			And I have pressed 'memory minus' operator button
			And I have pressed 'memory recall' operator button
		Then I verify that the result is 60