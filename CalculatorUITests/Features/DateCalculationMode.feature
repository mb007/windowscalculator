﻿Feature: Use date calculation mode

Scenario: Calculate difference between dates
	Given I have navigated to the date calculation mode
	When I have pressed From Date Button
		And I have selected day 1 from the date picker
		And I have pressed To Date Button
		And I have selected day 21 from the date picker
	Then I verify that the date calculation result is '2 tygodnie, 6 dni'