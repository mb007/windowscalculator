﻿using TechTalk.SpecFlow;

namespace CalculatorUITests.Hooks
{
    [Binding]
    public class LaunchCloseApp : CalculatorSession
    {
        private readonly ScenarioContext _scenarioContext;

        public LaunchCloseApp(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        [BeforeScenario]
        public void LaunchApp()
        {
            Setup(_scenarioContext);
        }

        [AfterScenario]
        public void CloseApp()
        {
            TearDown();
        }
    }
}
