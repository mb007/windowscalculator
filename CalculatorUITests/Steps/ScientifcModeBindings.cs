﻿using NUnit.Framework;
using TechTalk.SpecFlow;

namespace CalculatorUITests.Steps
{
    [Binding]
    public sealed class ScientifcModeBindings : CalculatorSession
    {
        // Selectors
        private string absButton = "absButton";
        private string negateButton = "negateButton";
        private string openParenthesisButton = "openParenthesisButton";
        private string closeParenthesisButton = "closeParenthesisButton";

        // Bindings
        [When(@"I have pressed scientific button: (absolute|negate|open parenthesis|close parenthesis)")]
        private void PressScientificButton(string buttonToPress)
        {
            string buttonSelector;

            switch (buttonToPress)
            {
                case "absolute":
                    buttonSelector = absButton;
                    break;
                case "negate":
                    buttonSelector = negateButton;
                    break;
                case "open parenthesis":
                    buttonSelector = openParenthesisButton;
                    break;
                case "close parenthesis":
                    buttonSelector = closeParenthesisButton;
                    break;
                default:
                    buttonSelector = null;
                    break;
            }

            session.FindElementByAccessibilityId(buttonSelector).Click();
        }
    }
}
