﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using OpenQA.Selenium.Appium.Windows;
using TechTalk.SpecFlow;

namespace CalculatorUITests.Steps
{
    [Binding]
    public sealed class StandardBindings : CalculatorSession
    {
        // Selectors //

        private string openOrCloseHamburgerMenu = "TogglePaneButton";
        private string standardModeButton = "Standard";
        private string scientificModeButton = "Scientific";
        private string programmerModeButton = "Programmer";
        private string dateCalculationModeButton = "Date";
        private string header = "Header";
        private string numZeroButton = "num0Button";
        private string numOneButton = "num1Button";
        private string numTwoButton = "num2Button";
        private string numThreeButton = "num3Button";
        private string numFourButton = "num4Button";
        private string numFiveButton = "num5Button";
        private string numSixButton = "num6Button";
        private string numSevenButton = "num7Button";
        private string numEightButton = "num8Button";
        private string numNineButton = "num9Button";
        private string decimalSeparatorButton = "decimalSeparatorButton";
        private string divideButton = "divideButton";
        private string multiplyButton = "multiplyButton";
        private string minusButton = "minusButton";
        private string plusButton = "plusButton";
        private string equalButton = "equalButton";
        private string calculatorResults = "CalculatorResults";
        private string clearButton = "clearButton";
        private string historyButton = "HistoryButton";
        private string historyListViewItemClassName = "ListViewItem";
        private string memoryPlusButton = "MemPlus";
        private string memoryMinusButton = "MemMinus";
        private string memoryRecallButton = "MemRecall";

        // Bindings //

        [Given(@"I have navigated to the (standard|scientific|programmer|date calculation) mode")]
        public void NavigateToChosenMode(string modeToSelect)
        {
            string nameOfCurrentMode = session.FindElementByAccessibilityId(header).Text;
            string nameOfModeToSelect;
            string modeButtonSelector;

            switch (modeToSelect)
            {
                case "scientific":
                    nameOfModeToSelect = "Naukowy";
                    modeButtonSelector = scientificModeButton;
                    break;
                case "programmer":
                    nameOfModeToSelect = "Programisty";
                    modeButtonSelector = programmerModeButton;
                    break;
                case "date calculation":
                    nameOfModeToSelect = "Obliczanie daty";
                    modeButtonSelector = dateCalculationModeButton;
                    break;
                default:
                    nameOfModeToSelect = "Standardowy";
                    modeButtonSelector = standardModeButton;
                    break;
            }

            if (nameOfCurrentMode != nameOfModeToSelect)
            {
                session.FindElementByAccessibilityId(openOrCloseHamburgerMenu).Click();
                session.FindElementByAccessibilityId(modeButtonSelector).Click();
            }
        }

        [When(@"I have pressed '(/|\*|-|\+|=|cancel|memory plus|memory minus|memory recall)' operator button")]
        private void PressCommonOperatorButton(string buttonToPress)
        {
            PressButton(buttonToPress);
        }

        [When(@"I have entered number (\d*\.?\d*$)")]
        public void EnterNumber(string numberToEnter)
        {
            foreach (char c in numberToEnter)
            {
                PressButton(c.ToString());
            }
        }

        [When(@"I have opened history")]
        public void OpenHistory()
        {
            session.FindElementByAccessibilityId(historyButton).Click();
        }

        [When(@"I have selected history list item number (.*)")]
        public void SelectHistoryListViewItem(int itemNumber)
        {
            ReadOnlyCollection<WindowsElement> listOfElements = session.FindElementsByClassName(historyListViewItemClassName);
            listOfElements[itemNumber-1].Click();
        }

        [Then(@"I verify that the result is (.*)")]
        public void VerifyResults(string expectedValue)
        {
            string resultValue = session.FindElementByAccessibilityId(calculatorResults).Text;
            string actualValuePrityfied = resultValue.Replace("Wyświetlana wartość to", "").Trim();
            Assert.AreEqual(expectedValue, actualValuePrityfied, "The Result is not correct");
        }

        // Helper methods //

        private void PressButton(string buttonToPress)
        {
            string buttonSelector;

            switch (buttonToPress)
            {
                case "0":
                    buttonSelector = numZeroButton;
                    break;
                case "1":
                    buttonSelector = numOneButton;
                    break;
                case "2":
                    buttonSelector = numTwoButton;
                    break;
                case "3":
                    buttonSelector = numThreeButton;
                    break;
                case "4":
                    buttonSelector = numFourButton;
                    break;
                case "5":
                    buttonSelector = numFiveButton;
                    break;
                case "6":
                    buttonSelector = numSixButton;
                    break;
                case "7":
                    buttonSelector = numSevenButton;
                    break;
                case "8":
                    buttonSelector = numEightButton;
                    break;
                case "9":
                    buttonSelector = numNineButton;
                    break;
                case ".":
                    buttonSelector = decimalSeparatorButton;
                    break;
                case "/":
                    buttonSelector = divideButton;
                    break;
                case "*":
                    buttonSelector = multiplyButton;
                    break;
                case "-":
                    buttonSelector = minusButton;
                    break;
                case "+":
                    buttonSelector = plusButton;
                    break;
                case "=":
                    buttonSelector = equalButton;
                    break;
                case "cancel":
                    buttonSelector = clearButton;
                    break;
                case "memory plus":
                    buttonSelector = memoryPlusButton;
                    break;
                case "memory minus":
                    buttonSelector = memoryMinusButton;
                    break;
                case "memory recall":
                    buttonSelector = memoryRecallButton;
                    break;
                default:
                    buttonSelector = null;
                    break;

            }

            session.FindElementByAccessibilityId(buttonSelector).Click();
        }
    }
}
