﻿using NUnit.Framework;
using TechTalk.SpecFlow;

namespace CalculatorUITests.Steps
{
    [Binding]
    public sealed class ProgrammerModeBindings : CalculatorSession
    {
        // Selectors

        private string hexButton = "hexButton";
        private string decimalButton = "decimalButton";
        private string octolButton = "octolButton";
        private string binaryButton = "binaryButton";

        // Bindings

        [Then(@"I verify that the value of HEX is (.*)")]
        public void VerifyValueOfHex(string expectedValue)
        {
            string hexValue = session.FindElementByAccessibilityId(hexButton).Text;
            string actualValuePprettified = hexValue.Replace("Wartość szesnastkowa", "").Trim();
            Assert.AreEqual(expectedValue, actualValuePprettified, $"The value of HEX is not correct. Expected value: {expectedValue} but was {actualValuePprettified}");
        }

        [Then(@"I verify that the value of DEC is (.*)")]
        public void VerifyValueOfDecimal(string expectedValue)
        {
            string decValue = session.FindElementByAccessibilityId(decimalButton).Text;
            string actualValuePprettified = decValue.Replace("Wartość dziesiętna", "").Trim();
            Assert.AreEqual(expectedValue, actualValuePprettified, $"The value of DEC is not correct. Expected value: {expectedValue} but was {actualValuePprettified}");
        }

        [Then(@"I verify that the value of OCT is (.*)")]
        public void VerifyValueOfOctol(string expectedValue)
        {
            string octValue = session.FindElementByAccessibilityId(octolButton).Text;
            string actualValuePprettified = octValue.Replace("Wartość ósemkowa", "").Trim();
            Assert.AreEqual(expectedValue, actualValuePprettified, $"The value of OCT is not correct. Expected value: {expectedValue} but was {actualValuePprettified}");
        }

        [Then(@"I verify that the value of BIN is (.*)")]
        public void VerifyValueOfBinary(string expectedValue)
        {
            string binValue = session.FindElementByAccessibilityId(binaryButton).Text;
            string actualValuePprettified = binValue.Replace("Wartość binarna", "").Trim();
            Assert.AreEqual(expectedValue, actualValuePprettified, $"The value of BIN is not correct. Expected value: {expectedValue} but was {actualValuePprettified}");
        }

    }
}
