﻿using NUnit.Framework;
using TechTalk.SpecFlow;

namespace CalculatorUITests.Steps
{
    [Binding]
    public sealed class DateCalculationModeBindings : CalculatorSession
    {
        // Selectors

        private string fromDateButton = "DateDiff_FromDate";
        private string toDateButton = "DateDiff_ToDate";
        private string dayNumberClassName = "CalendarViewDayItem";
        private string resultTextBlock = "DateDiffAllUnitsResultLabel";

        // Bindings

        [When(@"I have pressed From Date Button")]
        public void PressFromDateButton()
        {
            session.FindElementByAccessibilityId(fromDateButton).Click();
        }

        [When(@"I have pressed To Date Button")]
        public void PressToDateButton()
        {
            session.FindElementByAccessibilityId(toDateButton).Click();
        }

        [When(@"I have selected day (\d{1,2}) from the date picker")]
        public void SelectDayFromTheDatePicker(string dayNumber)
        {
            session.FindElementByClassName(dayNumberClassName)
                .FindElementByName(dayNumber)
                .Click();
        }

        [Then(@"I verify that the date calculation result is '(.*)'")]
        public void VerifyDateCalculationResult(string expectedResult)
        {
            string actualResult = session.FindElementByAccessibilityId(resultTextBlock).Text;
            Assert.AreEqual(expectedResult, actualResult, $"Date calculation result is not correct. Expected {expectedResult}, but was {actualResult}");
        }
    }
}