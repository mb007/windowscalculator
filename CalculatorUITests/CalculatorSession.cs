﻿using System;
using NUnit.Framework;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using TechTalk.SpecFlow;

namespace CalculatorUITests
{
    public class CalculatorSession
    {
        private const string WinAppDriverUrl = "http://127.0.0.1:4723/";
        private const string CalculatorAppId = "Microsoft.WindowsCalculator_8wekyb3d8bbwe!App";

        protected static WindowsDriver<WindowsElement> session;

        public static void Setup(ScenarioContext context)
        {
            if (session == null)
            {
                AppiumOptions options = new AppiumOptions();
                options.AddAdditionalCapability("app", CalculatorAppId);
                session = new WindowsDriver<WindowsElement>(new Uri(WinAppDriverUrl), options);

                Assert.IsNotNull(session);
                Assert.IsNotNull(session.SessionId);
            }
        }

        public static void TearDown()
        {
            if (session != null)
            {
                session.Quit();
                session = null;
            }
        }
    }
}